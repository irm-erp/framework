<?php

return [

	"auth" => [
	    "permission" => [
	        "enable" => env("ENABLE_PERMISSION", false),
	        "login_as" => env("LOGIN_AS"),
	    ],
	],

	"format" => [
		"number" => [
			"currency" => "",
			"decimals" => 0,
			"decimals_sep" => ",",
			"thousands_sep" => ".",
		],
	],

	"menu" => [
		// [db, collection]
	    "default" => "collection",

		"drivers" => [
			"db" => "pgsql",
		],

	    "migrate" => false,
	],

	"modular" => [
		"assets" => [
			"autoload" => env("MODULAR_ASSETS_AUTOLOAD", true),
		],

		"enable" => false,

		// Map URL
		"map" => [],

		// Will register modules on these path
		"path" => [
			"app/Modules" => "App\\Modules\\",
			"vendor/ramdhanmy27/laraden-framework/modules" => "Laraden\\Modules\\",
		],

		"separation" => [
			"menu" => true,
			"schema" => true,
		],
	],

	"storage" => [
		"config" => [
			"path" => storage_path("app/config"),
		],
	],

	// to publish theme
	"theme" => env("MIX_THEME", "freelancer"),
	
];
