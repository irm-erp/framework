/**
 * Show error modal
 * 
 * @param  String  message
 * @param  String  title  
 */
fn.alertError = function(message, title) {
    var modal = $("#error-alert");

    modal.modal("show");

    if (title !== undefined)
        $(".modal-title", modal).html(title);

    if (message !== undefined)
        $(".modal-body", modal).html(message);
}

/**
 * Show alert Modal
 * 
 * @param  String  message
 * @param  String  title  
 */
fn.alert = function(message, title) {
    var modal = $("#modal-basic");

    modal.modal("show");

    $(".modal-title", modal).html(title || "Info");

    if (typeof message !== 'string') {
        var msg = "";

        for (var i in message)
            msg += "<li>"+ message[i] +"</li>"

        $(".modal-body", modal).html(msg);
    }
    else $(".modal-body", modal).html(message);
}

/**
 * Show Confirmation Modal
 * 
 * @param  object  opt
 */
fn.confirm = function(opt) {
    var modal = $(opt.selector || "#modal-confirm");

    // show modal event
    if (!modal.hasClass("in")) {
        modal.find(".modal-title").html(opt.title || "Warning!");
        modal.find(".modal-body").html(opt.body || "Are you Sure ?");
        modal.modal("show");

        var accept = false;

        // yes
        modal.find('.modal-accept').unbind("click").bind("click", function(e) {
            accept = true;

            if (opt.hasOwnProperty("yes"))
                opt.yes(e);
        });

        // no
        modal.off("hidden.bs.modal").on("hidden.bs.modal", function(e) {
            if (accept===false && opt.hasOwnProperty("no"))
                opt.no(e)
        })
    }

    return modal;
}

/**
 * show flash messages alert
 * @param  String|Array messages
 * @param  String type
 */
fn.flash = function(messages, type) {
    if (!(messages instanceof Array))
        messages = [messages];

    $("<div class='alert alert-"+ (type || "warning") +"' role='alert'>"
            +"<button type='button' class='close' data-dismiss='alert'> <span>&times;</span> </button>"
            +"<li>"+ messages.join("</li><li>") +"</li>"
        +"</div>"
    ).hide().insertAfter(".page-header").fadeIn();
}