fn.event.push("bootstrap", function(scope) {
  // tooltip
  $('[tooltip]', scope).each(function() {
    var $this = $(this);
    var title = $this.attr("tooltip");

    if (!fn.empty(title))
      $this.attr("title", title);

    $this.tooltip();
  })

  // Modal
  $("[modal],[modal-sm],[modal-md],[modal-lg]", scope).click(function(e) {
    // must left button
    if (e.which != 1)
      return true;
    
    var button = $(this);
    var content = button.attr('modal-content') || '.page-content';
    var title = button.attr('modal-title') || $("title").text();
    var href = button.attr("href");

    // size
    var sizeSet = ["modal","modal-sm","modal-md","modal-lg"];

    for (var i in sizeSet) {
      // attribute is exists
      if (button.attr(sizeSet[i])!==undefined) {
        var target = $(button.attr(sizeSet[i]) || '#modal-basic');

        // set modal size
        if (sizeSet[i]!='modal')
          $(".modal-dialog", target).addClass(sizeSet[i]);
        break;
      }
    }

    target.find(".modal-title").html(title);
    target.modal("show");

    if (href != undefined) {
      var body = target.find(".modal-body");
      var loading = $("#img-loading").clone();

      loading.removeAttr("id");
      loading.css({"margin-top": "100px"});
      body.html($("<div class='text-center' style='min-height:300px'>").append(loading));

      $.ajax({
        url: href,
        success: function(res) {
          var content = $(res).find(content);

          if (content.length == 0)
            content = $(res);

          // recompile angular if exists
          /*if (isAngular) {
            angular.element(document).injector().invoke(function($compile) {
              var scope = angular.element(body).scope();
              $compile(body)(scope);
            })
          }*/

          body.html(content);
          fn.event.init(content);
        }
      })
    }

    e.preventDefault();
  });

  // Window Confirm
  $("[confirm]", scope).click(function(e) {
    if (e.which!=1)
      return true;

    var $this = $(this);
    var modal = fn.confirm({
      title: $this.attr("confirm-title"),
      body: $this.attr("confirm"),
      yes: function() {
        $this[0].click();
      }
    });

    if (!modal.hasClass("in")) {
      e.stopImmediatePropagation();
      return false;
    }
  })
});