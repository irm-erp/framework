fn.event.push("ui", function(scope) {
  // Datatables 
  if (!$.fn.DataTable.isDataTable("[datatable]")) {
    $("[datatable]", scope).each(function() {
      var datatable = $(this);
      var url = datatable.attr("datatable");

      // non-ajax datatable
      var options = {
        bInfo: false,
        bPaginate: datatable.attr("dt-paginate")!=="false",
        bFilter: datatable.attr("dt-search")!=="false",
        bLengthChange: datatable.attr("dt-length")!=="false",
        iDisplayLength: datatable.attr("dt-display") || 25,
        initComplete: function(settings, json) {
          // datatable.find("dt-template").remove();
        },
      };

      if (fn.empty(url)) {
        return $(this).DataTable(options);
      }

      // ajax datatable
      var column = [];

      datatable.find("[dt-field],[dt-col]").each(function() {
        var $this = $(this);
        var name = $this.attr("dt-field");

        fn.button = {};

        if (!!$(this).attr('dt-col')) {
          var content = datatable.parent()
            .find("dt-template "+ $this.attr("dt-col"))
            .html();

          column.push({
            bSearchable: $this.attr("search")!=="false",
            bSortable: $this.attr("sort")!=="false",
            mRender: function(data, type, row) {
              if (fn.empty(content))
                return;

              for (var i in row) {
                eval('var '+i+' = row[i];');
              }

              return content.replace(/\[\[(.+?)\]\]/g, function(match, syntax) {
                eval('var result = '+syntax);

                return fn.empty(result) ? "" : result;
              });
            }
          })
        }
        else if (!!name) {
          var data_column = $this.attr("dt-column");
          
          column.push({
            data: !!data_column ? data_column : name.substr(name.indexOf(".")+1),
            name: name,
            bSearchable: $this.attr("search")!=="false",
            bSortable: $this.attr("sort")!=="false",
          });
        }
      })

      datatable.DataTable($.extend(options, {
        processing: true,
        serverSide: true,
        ajax: url,
        columns: column,
      }));

      datatable.on("draw.dt", function() {
        fn.event.trigger("*", datatable);
      });

      datatable.on("draw", function() {
        datatable.parent().find("dt-template").remove();
      });
    });
  }
});