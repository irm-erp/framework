fn.csrf = $('meta[name="csrf-token"]').attr("content");

/**
 * Get value from array/object by key location (e.g: path.to.value)
 * 
 * @param  array|object  data
 * @param  String  index  
 */
fn.get = function(data, index) {
    // data should be array or obejct
    if (data === undefined || data === null)
        return;

    var indexes = new String(index).split(".");
    var tmp = $.extend(true, {}, data);

    for (var i in indexes) {
        // trying to retrieve undefined index
        if (typeof tmp[indexes[i]] === "undefined")
            return;

        tmp = tmp[indexes[i]];
    }

    return tmp;
};