fn.event.push("jquery", function(scope) {
  // check - uncheck all
  $("[check-all]", scope).change(function() {
    var $this = $(this);
    var target = $$($this.attr("check-all"));

    target.prop("checked", $this.prop("checked"))
  });

  // Anchor method spoofing
  $("a[method]", scope).click(function(e) {
    e.preventDefault();
    
    var $this = $(this);
    var form = $("<form method='POST' action='"+ $this.attr("href") +"' class='hide'>"+
        +"<input name='_method' type='hidden' value='"+ $this.attr("method") +"'>"
        +"<input name='_token' type='hidden' value='"+ fn.csrf +"'>"
      +"</form>");

    $(document.body).append(form);
    
    form.submit();
  });
});