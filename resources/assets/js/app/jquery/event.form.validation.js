fn.event.push("form.validation", function(scope) {
  if (typeof Validator !== "undefined") {
    var v = {
      rules : {},

      validateInput: function(name, input) {
        var data = {};
        var rules = {};

        data[name] = input.val();
        rules[name] = this.rules[name];

        // undefined rule would trigger error on validate
        // catch error and ignore that shit
        var validator;
        var isValid = true;

        try { 
          validator = new Validator(data, rules);
          isValid = validator.passes() 
        } 
        catch (e) {}

        if (typeof isValid === "boolean") {
          var group = input.closest('.form-group');

          // input messages
          var input_group = group.find(".input-group");

          if (input_group.length > 0)
            input = input_group;

          var msg = input.parent().find(".help-block");

          if (msg.length == 0) {
            var msg_block = $("<p class='help-block'>");

            input.parent().append(msg_block)
          }

          // success
          if (isValid) {
            group.removeClass('has-error').addClass('has-success');
            msg.html("");
          }
          // fails
          else {
            group.removeClass('has-success').addClass('has-error');
            msg.html(validator.errors.first(name));
          }
        }

        return isValid;
      },
    };

    $("form", scope).each(function() {
      /*inputs*/
      var $this = $(this);
      var input = $this.find("[rules]");

      // initializing input and collecting rules
      input.each(function() {
        var $this = $(this);
        var name = $this.attr("name");
        var rule = $this.attr("rules");

        v.rules[name] = rule;

        // adding star to required element
        if (rule.indexOf("required") !== -1) {
          var label = $this.closest(".form-group").find(".control-label")

          if (!label.hasClass("label-marked")) {
            label.append("<b class='text-danger'> *</b>")
            label.addClass("label-marked")
          }
        }

        var validate = function() {
          return v.validateInput(name, $this);
        };

        // on change validation
        $this.change(validate);
        $this.keyup(validate);
      });

      /*Form*/
      // on submit validation
      $this.submit(function(e) {
        var isValid = true;

        input.each(function() {
          var $this = $(this);

          isValid = v.validateInput(this.name, $this);

          // validation fails
          if (!isValid) {
            $this.focus();
            e.preventDefault();
          }

          return isValid;
        });
      })
    });
  }
});