fn.event.push("form", function(scope) {
  // File input preview
  $("input[type='file'][preview]", scope).each(function() {
    var $this = $(this).clone();

    $("<div class='thumbnail mb-xs'>")
      .append($("<img>").attr("src", $this.attr("preview")))
      .insertBefore(this);
  });

  // Format number
  // change value to formatted number on keyup
  if (typeof $.fn.inputmask !== "undefined") {
    $("[num-format]", scope).each(function() {
      var $this = $(this);

      $this.inputmask("currency", {
        rightAlign: false, 
        prefix: "", 
        groupSeparator: ".",
        radixPoint: ",", 
        digits: $this.attr("num-format") || 0,
        removeMaskOnSubmit: true,
        autoGroup: true,
        onUnMask: function(maskedValue, unmaskedValue) {
          return unmaskedValue.replace(/\./g, "").replace(/\,/g, ".");
        },
      })
    });
  }

  // Select2
  if (typeof $.fn.select2 !== "undefined") {
    $("[select2]", scope).each(function() {
      var select2 = $(this);

      // add empty option, to fix placeholder problem
      select2.prepend("<option>");

      select2
        .css({width: "100%"})
        .select2({
          allowClear: true,
          multiple: select2.is("[multiple]"),
          placeholder: select2.is("[placeholder]") ? select2.attr("[placeholder]") : "Select..",
          escapeMarkup: function(markup) {
            return markup;
          }
        })
    })
  }

  // Bootstrap Date Picker
  if (typeof $.fn.datepicker !== "undefined") {
    $("[datepicker]", scope).each(function() {
      var $this = $(this);
      var input = $this.clone();
      var format = input.attr("datepicker");

      // prevent duplicate event
      if (input.data("datepicker")) {
        return;
      }

      input.datepicker({format: !!format ? format : "yyyy-mm-dd"});
      input.addClass("form-control")

      // adding calendar icon
      $("<div class='input-group'>")
          .append(input)
          .prepend("<span class='input-group-addon'><i class='fa fa-calendar-o'></i></span>")
          .insertBefore($this);

      $this.remove();
    });
  }

  // Bootstrap Datetime Picker
  if (typeof $.fn.datetimepicker !== "undefined") {
    $("[datetimepicker]", scope).each(function() {
      var $this = $(this);
      var input = $this.clone();
      var format = input.attr("datetimepicker");

      // prevent duplicate event
      if (input.data("datetimepicker")) {
        return;
      }

      input.datetimepicker({
        format: !!format ? format : "YYYY-MM-DD HH:mm:ss",
      });

      input.addClass("form-control")

      // adding calendar icon
      $("<div class='input-group'>")
          .append(input)
          .prepend("<span class='input-group-addon'><i class='fa fa-calendar-o'></i></span>")
          .insertBefore($this);

      $this.remove();
    });
  }

  // Bootstrap Date Picker
  if (typeof $.fn.datetimepicker !== "undefined") {
    var input_old = $("[datetimepicker]", scope);
    var input = input_old.clone();

    input.datetimepicker();
    input.addClass("form-control")

    // adding calendar icon
    $("<div class='input-group'>")
        .append(input)
        .prepend("<span class='input-group-addon'><i class='fa fa-calendar-o'></i></span>")
        .insertBefore(input_old);

    input_old.remove();
  }

  // Bootstrap Time Picker
  if (typeof $.fn.timepicker !== "undefined") {
    $("[timepicker]", scope).each(function() {
      var $this = $(this);
      var input = $this.clone();

      // prevent duplicate event
      if (input.data("timepicker")) {
        return;
      }

      input.timepicker();
      input.addClass("form-control")

      // adding calendar icon
      $("<div class='input-group'>")
          .append(input)
          .prepend("<span class='input-group-addon'><i class='fa fa-clock-o'></i></span>")
          .insertBefore($this);

      $this.remove();
    });
  }

  // Bootstrap Date Range Picker
  if (typeof $.fn.daterangepicker !== "undefined") {
    $("[daterangepicker]", scope).each(function() {
      var $this = $(this);
      var input = $this.clone();

      // prevent duplicate event
      if (input.data("datepicker")) {
        return;
      }

      input.daterangepicker({
        // autoUpdateInput: false,
        locale: {
          format: "YYYY/MM/DD"
        }
      });
      input.addClass("form-control")

      // adding calendar icon
      $("<div class='input-group'>")
          .append(input)
          .prepend("<span class='input-group-addon'><i class='fa fa-calendar'></i></span>")
          .insertBefore($this);

      $this.remove();
    });
  }

  // File Upload
  if (typeof $.fn.fileupload !== "undefined") {
    $("[fileupload]", scope).fileupload();
  }
});