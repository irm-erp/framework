@extends("layout.app")

@section("app-content")
  <!-- Page Sidebar -->
  <div class="page-sidebar sidebar-fixed" id="sidebar">
    {!! Menu::render() !!}
  </div>

  <!-- Page Content -->
  <div class="page-content">
    {!! Menu::renderBreadcrumb() !!}

    <div class="page-header position-relative">
      <div class="header-title"> <h1> @yield("title") </h1> </div>
      <div class="pull-right mt-xs mr-xl"> @yield("header-buttons") </div>
    </div>

    @hasSection("content")
      @include("layout.content")
    @endif
  </div>
@endsection

@section("app-title")
  <!-- Sidebar Collapse -->
  <div class="sidebar-collapse" id="sidebar-collapse">
    <i class="collapse-icon fa fa-bars"></i>
  </div>

  <!-- Title Header -->
  <div class="navbar-title" style="position: absolute; left: 245px;">
    <h3 class="mt-sm" style="color: #fff">@yield("title")</h3>
  </div>
@endsection
