@extends("layout.app")

@section("content")
  <div class="jumbotron text-center bg-none mt-xlg">
    <h1 style="font-size: 150px">@yield("code")</h1>
    <h2>@yield("message")</h2>

    <br> <hr>

    <a href="{{ url("/") }}" class="btn btn-lg btn-danger">
	    <i class="fa fa-home"></i> Back to Home
	  </a>
  </div>
@endsection
