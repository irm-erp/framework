@extends("auth.app")

@section("title", "Login")

@section("content-auth")
  <form action="{{ url("login") }}" method="post">
    {!! csrf_field() !!}

    <div class="form-group mb-lg {{ $errors->has('email') ? 'has-error' : '' }}">
      <label>Email</label>
      <div class="input-group input-group-icon">
        <input name="email" type="text" class="form-control input-lg" 
          value="{{ old('email') }}" />
        <span class="input-group-addon">
          <span class="icon icon-lg">
            <i class="fa fa-user"></i>
          </span>
        </span>
      </div>

      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group mb-lg {{ $errors->has('password') ? 'has-error' : '' }}">
      <div class="clearfix">
        <label class="pull-left">Password</label>
        <a href="{{ url('/password/reset') }}" class="pull-right">Lost Password?</a>
      </div>
      <div class="input-group input-group-icon">
        <input name="password" type="password" class="form-control input-lg" />
        <span class="input-group-addon">
          <span class="icon icon-lg">
            <i class="fa fa-lock"></i>
          </span>
        </span>
      </div>

      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>

    <div class="row">
      <div class="col-sm-8">
        {!! Form::checkbox("remember", 1, null, ["label" => "Remember Me"]) !!}
      </div>
      <div class="col-sm-4 text-right">
        <button type="submit" class="btn btn-primary">Sign In</button>
      </div>
    </div>
  </form>
@endsection