<center>
    <h1 style="font-family: arial">Verify Your Email</h1>

    <p style="font-family: arial; color: #444">
        Please click this button below to verify your email.
    </p>
    
    <br>
    <a href="{{ url("tss/verify/$tenant_code/".rawurlencode($code)) }}" style="
        font-family: arial;
    	padding: 10px 15px; 
    	background: #f90; 
    	font-weight: bold; 
    	color: #fff; 
    	border-radius: 3px; 
        border: none; 
    	text-decoration: none; 
    ">Verify</a>
</center>