@extends("layout.app")

@section("app-content")
	<div class="panel mt-xlg panel-default col-md-4 col-md-offset-4">
		<div class="panel-body">
			<h3 class="page-header mt-sm mb-xlg">@yield("title")</h3>
			@yield("content-auth")
		</div>
	</div>
@endsection