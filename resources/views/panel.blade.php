@extends("app")

@section("content")
  <div class="widget radius-bordered">
    <div class="widget-header">
      <span class="widget-caption">@yield("panel-title")</span>
    </div>
    <div class="widget-body bordered-top bordered-danger">
      <div class="collapse in">
        @yield("panel-content") 
      </div>
    </div>
  </div>
@endsection