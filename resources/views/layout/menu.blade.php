<ul id="menu" class="nav sidebar-menu"> {!! $menu !!} </ul>

@push("script")
    <script type="text/javascript">
        $("#menu .active").parents("li").addClass("open");
    </script>
@endpush