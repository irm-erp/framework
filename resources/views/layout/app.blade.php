<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>
    @hasSection("title")
      @yield("title")
    @else
      @section("title") App @show
    @endif
  </title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <link rel="shortcut icon" href="{{ asset("img/favicon.ico") }}" />
  <link rel="stylesheet" href="{{ asset("css/app.css") }}" />
  @stack("style")
</head>
<body>
  <!-- Navbar -->
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="navbar-container">
        <!-- Navbar Brand -->
        <div class="navbar-header pull-left">
          <a href="{{ url("/") }}" class="navbar-brand">
            <h4 class="ml-sm"> <i class="fa fa-rocket"></i> Applications </h4>
          </a>
        </div>

        @yield("app-title")

        {{-- Account Area --}}
        @if (Auth::check())
          <div class="navbar-header pull-right">
            <div class="navbar-account">
              <ul class="account-area">
                <li>
                  <a class="login-area dropdown-toggle" data-toggle="dropdown">
                    <div class="avatar" title="View your public profile">
                      <img src="{{ asset("img/user-default.jpg") }}" 
                        class="img-circle" />
                    </div>

                    <section>
                      <h2>
                        <span class="profile">
                          {{ Auth::user()->name }}
                        </span>
                      </h2>
                    </section>
                  </a>

                  <!--Login Area Dropdown-->
                  <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                    <li>
                      <a href="{{ url("logout") }}" method="post">
                        <i class="fa fa-power-off"></i> Sign out
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        @else
          <div class="navbar-header pull-right">
            <div class="navbar-account">
              <ul class="account-area">
                <li>
                  <a class="login-area" href="{{ url("login") }}">
                    <section> <h2> <span>
                      <i class="fa fa-user"></i> Login 
                    </span> </h2> </section>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>

  <div class="main-container container-fluid">
    <div class="page-container">
      @hasSection("app-content")
        @yield("app-content")
      @else
        @include("layout.content")
      @endif
    </div>
  </div>

  <!-- Modals -->
  @include("ui.modal", ["id" => "modal-basic"])
  @include("ui.modal", ["id" => "modal-error"])
  @include("ui.modal", [
    "id" => "modal-confirm", 
    "footer" => 
      "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>"
        .trans("action.yes")
      ."</button>
      <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>"
        .trans("action.no")
      ."</button>"
  ])

  <!-- Javascript -->
  <script src="{{ asset("js/app.js") }}"> </script>
  <script>
    fn.url.base = '{{ url("/") }}/';
    
    $(document).ready(function() {
      @if (Notif::exists())
        var notification = {!! json_encode(Notif::pull()) !!};

        for (var type in notification) 
          fn.notif(notification[type], type);
      @endif
    })
  </script>
  @stack("script")
</body>
</html>
