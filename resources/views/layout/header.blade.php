@extends("layout.app")

@section("content-layout")
  <header>
    <div class="container" id="maincontent" tabindex="-1">
      <div class="row">
        <div class="col-lg-12">
          @yield("content-header")
        </div>
      </div>
    </div>
  </header>
@endsection