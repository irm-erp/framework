<div class="page-breadcrumbs">
  <ul class="breadcrumb">
    <li> 
      <i class="fa fa-home"></i> 
      <a href="{{ url("/") }}"> Home </a> 
    </li>

    @foreach ($path as $menu)
      <li> 
        <a href="{{ $menu->url!==null ? url($menu->url) : "#" }}"> 
          <span>{{ trans_label($menu->title) }}</span>
        </a> 
      </li>
    @endforeach
  </ul>
</div>