Form Builder
https://laravelcollective.com/docs/5.4/html

##Form Input
###Text
```php
{!! Form::text('name', 'Maxlength', ["maxlength" => 10]) !!}
```
or 
```php
{!! Form::group('text', 'nama', ['label'=>'Nama']) !!}
```

###TextArea
```php
{!! Form::group('textarea', 'nama', ['label'=>'Nama']) !!}
```

###Date
```php
{!! Form::date('name', date("d/m/Y")) !!}
```
or 
```php
{!! Form::group('date', 'tanggal', ['label'=>'Tanggal', 'value'=> date("d/m/Y")]) !!}
```

##Date Range
```php
$to = $from = date("d/m/Y");

{!! Form::dateRange('name', [$from, $to]) !!}
```
or 
```php
$to = $from = date("d/m/Y");

{!! Form::group('date-range', 'tanggal', ['label'=>'Tanggal', 'value'=>[$from, $to]]) !!}

```

##Time
```php
{!! Form::time('name', 'Time') !!}
```
or 
```php
{!! Form::group('time', 'waktu', ['label'=>'Waktu']) !!}
```

##Checkbox
###Single
```php
{!! Form::checkbox('name', 'v1', true, ["class" => "colored-success"]) !!}
```

###Multiple
```php
{!! Form::checkboxes('name[]', ["v1" => "label 1", "v2" => "label 2"], ["v1"]) !!}
```
or
```php
{!! Form::group('checkboxes', 'hobby', [
    'options' => [
        'renang' => 'Renang',
        'basket' => 'Basket',
    ],
]) !!}
```

##Radio
###Single
```php
{!! Form::radio('name', 'v1', true, ["class" => "colored-blue"]) !!}
```

###Multiple
```php
{!! Form::radios('name', ["v1" => "label 1", "v2" => "label 2"], "v1") !!}
```
or
```php
{!! Form::group('radios', 'jenis_kelamin', [ 
    "options" => ['laki-laki','perempuan'],
]) !!}
```

##Toggle Switch
```php
{!! Form::toggleSwitch('name', "value", true, [], "switch-sm switch-dark") !!}
```

##Form Group
Gunakan Form Group jika anda ingin membuat form input beserta dengan labelnya. Seperti HTML di bawah ini.

```html
 <div class="form-group">
     <label for="name" class="col-md-3 control-label">Label</label>
     <div class="col-md-6">
         <input class="form-control valid" name="name" value="value" id="name" type="text">
     </div>
 </div>
```

Berikut contoh cara penggunaannya :

```php
{!! Form::group('date-range', 'name', 'Date Range') !!}
{!! Form::group('text', 'name', 'Maxlength', "value", ["maxlength" => 20]) !!}
```

##Validation
###Rules
Bisa juga ditambahkan manual seperti berikut.

```php
{!! Form::open() !!}
{!! Form::rules(["number" => "required|integer|max:30"]) !!}
{!! Form::group("text", "number") !!}
{!! Form::close() !!}
```

##Model
Jika anda sudah menuliskan rules pada model, akan sangat mudah dalam menggunakannya. 

```php
{!! Form::model($model /* atau bisa juga MyModel::class */) !!}
// form input..
{!! Form::close() !!}
```

Pada method model, anda bisa menginputkan instance model anda. Atau jika anda ingin menghemat memory, bisa juga memasukkan namespace model anda.