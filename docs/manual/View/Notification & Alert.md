#Notification
##Flash Messages

```php
use Flash;

Flash::push("Pesan Info", "info");
```

```php
use Html;

Flash::info("Information");
Flash::warning("Pesan Warning");
Flash::danger(["Pesan Error", "Pesan Error #2"]);
Flash::success(Html::icon("fa fa-check")." Pesan Success");
```

##Notification Alert
Hampir sama seperti Flash, hanya method danger diganti dengan error.

```php
use Notif;

Notif::push("you've got a Notification!", "error");
```

```php
Notif::error("Failed");
Notif::success("Success");
Notif::info([
    [
        "msg" => "you've got a Notification!", 
        "title" => "Information", 
        // "icon" => "fa fa-info", 
        // "class" => "bg-info",
        // "type" => "info",
    ]
]);
Notif::warning([
    [
        "msg" => "you've got a Warning!", 
        "title" => "Warning", 
        "icon" => "fa fa-exclamation-triangle", 
    ],
    [
        "msg" => "you've got a Warning!", 
        "title" => "Warning #2", 
        "icon" => "fa fa-home", 
    ]
]);
```

