#Menu
Untuk membuat menu navigasi pada halaman web, anda hanya cukup menambahkannya saja pada routing aplikasi. Aplikasi yang akan me-render data menu tersebut ke dalam bentuk html.

Sederhananya, menu dapat didefinisikan seperti di bawah.

```php
Menu::make("main", function($menu) {
    $menu->add("Missing");
    $menu->add("Missing", "path/to/url");
    $menu->add("Missing", "path/to/url", "fa fa-dollar");
    $menu->add("Label 1", "asdasd")->attr([
        "icon" => "fa fa-dollar", 
        "class" => 'text-danger',
    ]);
});
```

##Nested Menu
Routing menu juga bisa digunakan seperti fungsi `Route::group`. Anda bisa definisikan Middleware, Namespaces, Sub-Domain Routing dan Route Prefixes bersamaan dengan menu.

```php
Menu::make("main", function($menu) {
    $menu->add("Label 1.2", "path/to/url", "fa fa-user")
        ->child(function($menu) {
            $menu->add("Label 1.2.1", "/", "fa fa-building");
        });
});
```

##Example
```php
Menu::make("main", function($menu) {
    $menu->add("Label 1", "asdasd")
        ->attr(["icon" => "fa fa-dollar", "test" => 'asdf'])
        ->child(function($menu) {
            $menu->add("Label 1.1", "test", "fa fa-building");
            $menu->add("Label 1.2", "path/to/url", "fa fa-user")
                ->child(function($menu) {
                    $menu->add("Label 1.2.1", "/", "fa fa-building");
                });
        });

    $menu->add("Label 2", "path/to/url", "fa fa-arrow-up");
});
```
