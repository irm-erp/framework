#Helpers
##String Formatter
###xml_encode

Contoh Penggunaan:

```php
echo htmlentities(xml_encode(["data" => ["item" => [1, 2, 3]]]));

/* Hasil :
<data> 
  <item>1</item> 
  <item>2</item> 
  <item>3</item> 
</data>
*/
```

###currency
```php
currency(10000); // Rp 10.000
currency(10000, 2, "$"); // $ 10.000,00
```

##Debug
Karena framework ini merupakan turunan dari framework laravel, tentunya sudah disediakan fungsi `dd` untuk keperluan debugging. Akan tetapi apabila masih terasa kurang, sudah ditambahkan pula beberapa fungsi yang mendukung untuk mendebug program anda.

###prins 
Menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
prins($var_1, $var_2, ..., $var_n);
```

###prin
Menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
prin($var_1, $var_2, ..., $var_n);
```

###traces
Backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n`

```php
traces($var_1, $var_2, ..., $var_n);
```

###trace
Backtrace process dan menampilkan isi variable dari `$var_1` sampai `$var_n` dan `exit`

```php
trace($var_1, $var_2, ..., $var_n);
```

##Case Converter
###hyphen_case

```php
echo hyphen_case("CamelCase"); // camel-case
echo hyphen_case("object_id"); // object-id
```

###title_case

```php
echo title_case("object-id"); // Object Id
echo title_case("object_id"); // Object Id
echo title_case("CamelCase"); // Object Id
```

##View
###View
Penggunaan view pada biasanya disertai dengan lokasi file view dan data yang  ingin dimasukkan kedalam view dalam bentuk array.

```php
view("path.to.view", ["var1" => $data1]);
```

Tapi anda bisa menyingkatnya jika anda menggunakan Controller, seperti ini.

```php
class ViewController extends Controller 
{   
    public function getIndex() 
    {
        // sama dengan view("view.index", []);
        return view([]);
    }
    
    public function report() 
    {
        // sama dengan view("view.report", ["data" => [1, 2, 3]]);
        return view(["data" => [1, 2, 3]]);
    }
}
```

###View Theme

Pada framework ini sudah disediakan dua tema secara default (`front` dan `backend`). Untuk menggunakan salah satunya anda bisa tambahkan nama tema sebagai namespace pada path view seperti ini.

```php
// path: resources/theme/backend/views/home/index.blade.php
view("backend::home.index");
```

Atau jika ingin menggunakan layout luarnya saja bisa digunakan seperti ini.

```php
extends("backend::app")

@section("content")
    <div>Bla bla bla...</div>
    
    // path: resources/theme/backend/views/ui/modal.blade.php
    @include("backend::ui.modal")
@endsection
```

>__Note :__ Jika view aplikasi modular yang dieksekusi memiliki path yang sama secara global, gunakan namespace `app` (contoh: `app::view.path`) agar menggunakan view yang ada pada module.

####Assets

File assets javascript dan css juga ada yang terpisah untuk setiap tema. Untuk mendapatkan path file asset dari tema yang diinginkan, penggunaannya hampir sama seperti pada view.

```php
// path: public/js/app.js
asset("js/app.js")

// path: public/theme/front/js/app.js
asset("front::js/app.js")

// path: public/theme/backend/js/app.js
asset("backend::css/app.css")
```

####view_exists

```php
view_exists("path.to.view"); // boolean
```

####first_view
Hampir sama seperti fungsi view, hanya saja fungsi ini hanya merender salah satu dari view path yang diberikan. View yang ditampilkan adalah yang pertama ditemukan. Jika file view hanya tersedia "path.to.view2", maka view yang di render adalah "path.to.view2".

```php
first_view(["path.to.view", "path.to.view2"], ["data" => $data]);
```

##Array Helper
###is_assoc

```php
is_assoc([2, 1]); // false
is_assoc([1 => 2, 1]); // true
is_assoc(['a' => 1, 'b' => '2']); // true
```