#Validasi
##Form Builder
Validasi pada form input menggunakan form builder memiliki konsep yang sama dengan validasi client yang sudah dijelaskan pada bagian DOM event. Kita perlu menambahkan attribute `rules` pada form input disertai dengan rules nya. 

```php
{!! Form::group('text', 'number', ["rules" => "integer|min:8"]) !!}

// atau

{!! Form::text('number', null, ["rules" => "integer|min:8"]) !!}
```

##Model
###Rules
```php
class TableName extends \App\Models\Model
{
    protected $table = "table_name";
    protected $fillable = ["amount", "description"];
    
    public function rules() 
    {
        return [
            "id" => "required|integer",
            "amount" => "required|integer",
            "description" => "string",
        ];
    }
}
```

Proses validasi akan dilakukan saat model menyimpan data. Yaitu pada saat fungsi `save` dipanggil. Jika data yang divalidasi tidak valid, maka ia akan mengembalikan nilai `false`.

###Label
```php
class TableName extends \App\Models\Model
{
    protected $table = "table_name";
    protected $fillable = ["amount", "description"];
    
    public function attributeLabels() 
    {
        return [
            "id" => "ID",
            "amount" => "Amount",
            "description" => "Description",
        ];
    }
}
```

###BeforeSave
Jika anda ingin lebih leluasa dalam memvalidasi data, anda bisa gunakan method `beforeSave`. 

```php
class User extends \App\Models\Model 
{
    public function beforeSave() 
    {
        // if there's a new password, hash it
        if ($this->isDirty('password')) {
            $this->password = Hash::make($this->password);
        }

        return true;
        // or don't return anything, since only a boolean false will halt the operation
    }
}
```

###Validasi Input Data dengan Pesan Error
Saat anda memvalidasi data yang diinputkan oleh user melewati form HTML, ada saatnya kita ingin meredirect ke halaman form HTML kembali untuk menampilkan error validasi. Untuk melakukan hal tersebut, secara manual anda bisa melakukan throw exception dengan class `App\Exceptions\ValidatorException` disertai dengan validator yang dibuat. Jika itu terlalu merepotkan, anda bisa gunakan salah satu method berikut.

####createOrFail

```php
MyModel::createOrFail($data);
```

####updateOrFail

```php
$model = MyModel::find($id);
$model->updateOrFail($data);
```

####saveOrFail

```php
$model = new MyModel
$model->field = $field_data;

// or
// $model = MyModel::find($id);

$model->saveOrFail();
```

###Referensi
https://github.com/laravel-ardent/ardent
https://laravel.com/docs/5.2/validation#available-validation-rules
