#Installation
##Quick Start
`composer.json`

```
"minimum-stability": "dev",
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:ramdhanmy27/laraden-framework"
    }
]
```

```
composer require ramdhanmy27/laraden-framework:dev-master
```

##Providers
```php
'providers' => [
    //...
    Laraden\Providers\LaradenServiceProvider::class,
]
```

##Assets

```
php artisan vendor:publish --tag=theme
```

`.env`

```
MIX_THEME=beyond
```

```
npm install laravel-elixir gulp dotenv
// or
yarn add laravel-elixir gulp dotenv
```

```
gulp 
```

##Modular & Menu Routing
`app/Providers/RouteServiceProvider.php`

```php
protected function mapWebRoutes()
{
    Route::modular();
    //...
}
```

##Middleware
`Laraden\Http\Middleware\ViewResolver::class` : Ajax filter content
`Laraden\Http\Middleware\Logger::class` : Request Logger

`app/Http/Kernel.php`

```php
protected $middlewareGroups = [
    'web' => [
        // ....
        \Laraden\Http\Middleware\ViewResolver::class,
        \Laraden\Http\Middleware\Logger::class,
    ],
    // ....
];
```

##Model
validation, label

```php
use Laraden\DB\Model;

class MyModel extends Model
{
    //...
}
```

##Controller
auto view, auto permission.

```php
use Laraden\Support\Trait\LaradenController;

class Controller extends BaseController
{
    use LaradenController;
}
```

##Helpers
auto view, enchanced dd()
`bootstrap/autoload.php` include sebelum file `vendor/autoload.php`.

```php
define("VENDOR_DIR", __DIR__."/../vendor");

require VENDOR_DIR.'/ramdhanmy27/laraden-framework/helpers/override.php';
require __DIR__.'/../vendor/autoload.php';
```

##UNOCONV (Document Converter)
###windows
add libreoffice program (`C:\Program Files\LibreOffice 5\program`) folder path to environmet variable

###Linux
####Tambah variable path di apache
Centos / RedHat : /etc/sysconfig/httpd
Debian / Ubuntu : /etc/apache2/envvars
export PATH="$PATH"

#Customization
##Vendor Publish
###Modules
Secara default, module pada package Laraden sudah di load. 
Anda bisa melihatnya di index `module.path` pada file `config/laraden.php` 
terdapat `"vendor/laraden/laraden/modules" => "Laraden\\Modules\\"`. 
Namun apabila ingin mengcustomnya, mungkin anda perlu menduplikasinya dengan perintah berikut. 
Module yang terpublish akan berada di folder `app/Modules`

```
php artisan vendor:publish --tag=modules
```

###Dokumentasi
Anda bisa mempublish file dokumentasi dengan perintah berikut.

```
php artisan vendor:publish --tag=docs
```

###Theme (View & Assets)
`.env`

```
MIX_THEME=beyond
```

```
php artisan vendor:publish --tag=theme
```

##Exception Handler
```php
use Laraden\Exceptions\ServiceException;
use Laraden\Exceptions\ValidatorException;

protected $dontReport = [
    // ....

    ServiceException::class,
];

public function render($request, Exception $exception)
{
    switch (get_class($exception)) {
        case ServiceException::class:
            return $exception->response();

        case ValidatorException::class:
            \Flash::danger($exception->getMessages()->all());
            return back()->withInput($request->input());
    }

    return parent::render($request, $exception);
}
```
