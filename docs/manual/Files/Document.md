#Document
https://github.com/PHPOffice/PHPWord

##Simple
```php
$convert = new Convert("path/to/report.docx");

$convert->data([
    "code" => "ab/0001",
    "total" => 100000,
]);

$file = $convert->to("pdf", "report.pdf", true); // override report.pdf if any

return response()->file($file);
```

##Using Template Processor
```php
$convert = new Convert("path/to/report.docx");

$convert->data(function($tpl) {
    $tpl->setValue("code", "ab/0001", 1);
    $tpl->setValue("total", 100000, 1);
});

$file = $convert->to("pdf", "report.pdf");

return response()->download($file);
```
