#JSON Storage

##Set Value
```php
file_config("path.to.key", "value");
```

##Get Value
```php
file_config("path.to.key"); // return "value"
```

##Set Multiple Values
```php
file_config([
    "path.to.key1" => "value 1",
    "path.to.key2" => "value 2",
    "path.to.key3" => "value 3",
]);
```
