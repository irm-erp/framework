#Permissions
##Registrasi
Berikut contoh untuk meregistrasikan permissions pada sebuah controller

```php
class ContohController extends Controller 
{
    public static $permissions = [
        ["create", "Tambah Data"],
        ["update"],
        "delete",
    ];
}
```

Anda dapat memilih metode manapun yang anda suka. Pada index pertama isikan nama permision lalu isikan juga deskripsi pada index selanjutnya. Atau cukup isikan nama permisionnya saja dan controller yang akan mengurus sisanya.

##Penggunaan

Permission yang sudah diregistrasikan akan tesimpan dalam database dengan format `contoh:create`. Jika anda ingin mengecek apakah user memiliki hak akses `create` pada `ContohController`. Anda bisa tuliskan sebagai berikut :

```php
if (Auth::user()->can('contoh:create')) {
    // eksekusi create
}
```

Namun apabila itu terlalu panjang untuk anda, bisa juga bisa dituliskan seperti ini:

```php
// nama permissions mengikuti controller yang sedang diakses
if (can('create')) {
    // eksekusi create
}
```