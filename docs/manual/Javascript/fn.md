#fn
##url
Jika anda terlalu malas untuk menuliskan url yang panjang dengan http query yang banyak anda bisa menggunakan fungsi ini.

```javascript
fn.url("pegawai", {q: "budi", sort:"asc"})
// hasil: http://domain.com/pegawai?q=budi&sort=asc
```

##format
###num
Memformat angka menjadi format uang (seperti: 10.000).

```javascript
var money = fn.format.num("10000")
// money -> 10.000
```

##empty
Memeriksa apakah variable kosong atau tidak.

```javascript
fn.empty(undefined) // true
fn.empty(null) // true
fn.empty(" ") // true
fn.empty({}) // true
fn.empty([]) // true

fn.empty(0) // false
fn.empty("string") // false
fn.empty([1,2,3]) // false
fn.empty({a: 1, b: 2}) // false
```

##isset
Object yang memiliki banyak level anak kadang merepotkan untuk diperiksa. Dengan fungsi ini anda dapat melakukannya seperti berikut :

```javascript
var obj = {
    a : {
        aa: 1, 
        ab: { 
            aba: 1 
        }
    }
}

fn.isset(obj.a.aa) // true
fn.isset(obj.a, "ab", "aba") // true
fn.isset(0) // true
fn.isset("") // true
fn.isset([]) // true
fn.isset({}) // true

fn.isset(obj.a.a, "abc") // false
fn.isset(obj, "a", "ab", "abc") // false
fn.isset(obj2) // false
fn.isset(undefined) // false
fn.isset(null) // false
```

##csrf
berisi nilai token csrf

```javascript
console.log(fn.csrf);
```

##notif
Menampilkan Pesan Notifikasi

```javascript
// success | danger | info | warning/notice
fn.notif("Notification Messages", "info");
fn.notif(["Notification #1", "Notification #2"], "warning");

// dark | primary
fn.notif({
    title: "Nightmare :3",
    msg: "Dark Messages",
    icon: "fa fa-user",
    class: "notification-dark",
});

fn.notif([
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
    {
        title: "Primary #1",
        msg: "Messages",
        icon: "fa fa-home",
        class: "notification-primary",
    },
]);
```
