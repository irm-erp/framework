#UI
##alertError
Menampilkan modal error.

```javascript
fn.alertError("Messages", "Title");
```

##alert
Menampilkan alert modal.

```javascript
fn.alert("Messages", "Title");

fn.alert(["Messages1", "Messages2"], "Title");
// <li>Messages1</li> <li>Messages2</li>

fn.alert({a: "Messages1", b: "Messages2"}, "Title");
// <li>Messages1</li> <li>Messages2</li>
```

##confirm
Menampilkan modal konfirmasi

```javascript
fn.confirm({
    // secara [default: #modal-confirm]
    selector: "#modal-confirm",
    
    // Judul Modal [default: "Warning!"]
    title: "Title",
    
    // Content Modal [default: "Are you Sure ?"]
    body: "Messages",
    
    // dieksekusi saat user menekan tombol setuju
    yes: function() {
        alert("Confirmed");
    },
    // dieksekusi saat user menekan tombol batal
    no: function() {
        alert("Canceled");
    },
});
```

##flash

Menampilkan flash message

```javascript
// success | danger | info | warning
fn.flash("Messages", "info");
fn.flash(["Messages #1", "Messages #2"], "danger");
```
