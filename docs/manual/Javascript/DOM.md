#DOM Event
Akan sangat merepotkan jika kita harus menuliskan event javascript pada setiap halaman. Dan akan sangat tidak efisien apabila event tersebut sudah pernah kita buat sebelumnya. Oleh karena itu, framework ini menyediakan event-event javascript untuk anda pakai. Anda tidak perlu lagi menuliskan javascript pada setiap halaman yang anda temui. Cukup tambahkan selector dan attribut yang dibutuhkan saja.

##Input
###Select2

```html
<select select2>
    <option value="1">Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```

####Multiple Select

```html
<select select2 multiple>
    <option value="1" selected>Opt 1</option>
    <option value="2" selected>Opt 2</option>
</select>
```
>https://select2.github.io/

####Datepicker

```html
<input type="text" datepicker>
```

####Date Range

```html
<input type="text" datepicker-range>
```
>http://bootstrap-datepicker.readthedocs.org/en/stable/

####Timepicker

```html
<input type="text" timepicker>
```

####Toggle Switch

```html
<input type="checkbox" switch>
```

####Number Format

```html
<input type="text" num-format>
```

## Modal
###.1. Membuat modal
Untuk membuat modal javascript anda perlu menambahkaan htmlnya sendiri ke dalam view. Bentuk html itu sendiri sama seperti html untuk membuat modal dengan menggunakan bootstrap. Karena memang bootstrap yang digunakan :3.
Jika menuliskan htmlnya terlalu panjang bagi anda atau anda lupa susunan html-nya, anda bisa tuliskan seperti ini.

```php
include("ui.modal", [
    "id" => "my-modal-selector",
    "class" => "modal-sm",
    "attr" => ["style" => "font-weight: bold"],
    "title" => "Judul Modal",
    "body" => "<div>Content.</div>",
    "footer" => 
        '<button type="button" data-dismiss="modal" class="btn btn-default">Tidak</button>
        <button type="button" class="btn btn-primary modal-accept">Ya</button>',
])
```

Jika anda tidak memerlukan salah satu dari index diatas, anda tidak harus mengisinya dengan `null` atau string kosong `""` cukup hiraukan saja. Karena tidak ada satupun yang bersifat required.

###.2. Pengunaan Modal
####a. Basic
Untuk menggunakan modal yang sudah anda buat, anda cukup menambahkan attribut `modal` dan tentukan selector element modal yang akan dieksekusi. Berikut contoh penggunaannya.

```html
<button modal='#my-modal-selector'>Show Modal</button>
```

Ketika anda meng-klik tombol tersebut, modal dengan id `my-selector-modal` akan ditampilkan. Apabila anda tidak mengatur value pada attribut modal, secara default valuenya adalah `#modal-basic`.

####b. Mengatur Title Modal
Title pada modal akan otomatis menggunakan title halaman yang sedang anda akses. Jika anda ingin menggunakan title yang lain, gunakan attribut `modal-title`.

```html
<button modal='#my-modal-selector' modal-title='Judul Modal'>Show Modal</button>
```

####c. Mengatur Body Modal
Body pada modal bisa anda atur dengan attribut `modal-body`.

```html
<button modal='#my-modal-selector' modal-body='Badan Modal'>Show Modal</button>
```

####d. Ukuran Modal
Modal bootstrap menyediakan tiga ukuran pada modal, yaitu `sm`, `md`, dan `lg`. Untuk menentukan ukuran modal yang ingin anda tampilkan anda bisa gunakan seperti ini :

```html
<button modal-sm='#my-modal-selector'>Show Modal</button>
```

####e. Modal AJAX
Jika anda menambahkan attribut modal pada element `a` yang memiliki attribute `href`, content modal yang ditampilkan pada saat anda meng-kliknya adalah content halaman url pada attribute href tersebut. 

```html
<a href="url-content" modal="#modal-basic">Show Modal</button>
<!-- atau kosongkan nilai dari attribute 'modal'. Karena secara default akan mengarah ke #modal-basic -->
```
Pada saat modal melakukan request ajax pada url tersebut, aplikasi akan merespon dengan kode HTML yang mendeskripsikan halaman secara keseluruhan. Sayangnya hal tersebut tidak dibutuhkan. Kita hanya butuh kontennya saja, tanpa header, sidebar, ataupun footer. Untuk menyiasati hal tersebut, anda bisa atur pada source view yang diakses pada halaman tersebut seperti ini.


```html
extends('app')

section('content')
    <div class="panel">
        <div class="panel-heading">
            <h2 class="panel-title">Contoh Transaction</h2>
        </div>

        <div class="panel-body">
        @section("content-ajax")
            {!! Form::model(@$model) !!}
                {!! Form::group('text', 'amount', 'Amount') !!}
                {!! Form::group('select', 'provinsi_id', 'Provinsi', Provinsi::lists("name", "id")) !!}
                {!! Form::group('select', 'kota_id', 'Kota', Kota::lists("name", "id")) !!}
                {!! Form::group('textarea', 'description', 'Description') !!}

                <div class="col-md-offset-3">
                    {!! Form::submit("Simpan", ["name" => "save"]) !!}
                    {!! Form::submit("Kirim", ["name" => "sent", "class" => "btn btn-warning"]) !!}
                </div>
            {!! Form::close() !!}
        @show
        </div>
    </div>
endsection

```

Jika anda mendefinisikan section `content-ajax` pada view, aplikasi akan merespon dengan kode HTML yang berada pada section tersebut. Jika anda tidak mendefinisikannya, respon aplikasi akan mengambil dari section `content`. Jadi anda tidak perlu repot-repot memisah file view atau memfilternya di javascript.
>Catatan: Hanya berlaku pada request ajax.

###.3. Predefined Modal
Pada framework ini sudah disediakan beberapa modal sebagai berikut.

####a. Modal Basic
Modal ini merupakan modal default yang akan digunakan jika anda tidak mengatur selector pada attribut modal. Modal ini hanya menampilkan header dan body saja, tanpa footer. Modal ini digunakan secara default digunakan untuk menampilkan pesan alert atau content yang di-load oleh AJAX.

```html
<a href="url-content" modal="#modal-basic">Show Modal</a>
```

####b. Modal Confirm
Jika anda membutuhkan konfirmasi action terhadap user, anda bisa menggunakan modal ini. Modal ini disediakan footer dengan dua tombol. Ya dan Tidak.

```html
<a href="delete/12" confirm="Apakah anda yakin ?">Hapus</a>
```

####c. Modal Error
Anda bisa menggunakan modal ini untuk menampilkan pesan error yang terjadi pada javascript. Modal ini menyediakan tombol untuk close dan mereload ulang halaman.

##Datatable
####a. Basic
Untuk membuat datatable, anda cukup menambahkan saja attribute `datatable` pada element `table`. Berikut contohnya.

```html
 <table class="table table-bordered" datatable dt-paginate="false">
     <thead>
         <tr>
             <th>ID</th>
             <th>Amount</th>
             <th>Created At</th>
             <th>Updated At</th>
         </tr>
     </thead>
     <tbody>
        <tr>
             <td>Value of ID</td>
             <td>Value of Amount</td>
             <td>Value of Created At</td>
             <td>Value of Updated At</td>
        </tr>
     </tbody>
 </table>
```
Pada contoh diatas anda dapat melihat attribut `dt-paginate="false"`. Attribut tersebut akan mendisable fitur pagination pada datatable. Jika anda ingin mengaktifkannya, atur nilainya menjadi `true` atau hapus attribut tersebut. Karena fitur pagination akan aktif secara default.

####b. Datatable AJAX
Berikut contoh pembuatan AJAX Datatable.

#####Server Side

```php
use Datatables;
//...

// Simple
public function anyTransactionData($status) {
    // membuat respon datatable
    return Datatables::of(
        DB::table("transaction")->where("status", $status)
    )->make(true);
}

// Dengan custom data
public function anyTransactionData($status) {
    // membuat respon datatable
    $res = Datatables::of(
        DB::table("transaction")->where("status", $status)
    )->make(true);

    // custom content
    $content = $res->getData();
    $content->data = array_map(function($data) use ($status) {
        $data->action = "custom action button";

        return $data;
    }, $content->data);

    return $res->setData($content);
}
```
Pada sisi server, gunakan facade `Datatables` untuk membuat response AJAX. Anda tidak perlu mengkhawatirkan tentang searching, sorting maupun paging pada data. 
>Untuk lebih detail penggunaan datatable, anda bisa kunjungi halaman berikut. https://github.com/yajra/laravel-datatables/wiki

#####Client Side
Setelah anda membuat halaman untuk merespons request datatable, tambahkan url yang mengarah pada halaman tersebut pada attribute `datatable`. Lalu berikan pula attribute `dt-field` dengan nilainya adalah nama index pada data yang anda berikan pada response. Biasanya adalah nama kolom pada table. Anda pun bisa mendisable fitur sorting dan pencarian jika perlu. Cukup tambahkan attribute `sort` dan `search` dengan nilai `false`.

```html
 <table class="table table-bordered" datatable="{!! url("contoh-transaction/transaction-data/draft") !!}">
    <thead>
        <tr>
            <th dt-field="id">ID</th>
            <th dt-field="amount">Amount</th>
            <th dt-field="created_at">Created At</th>
            <th dt-field="updated_at">Updated At</th>
            <th dt-action="#action" sort="false" search="false">Actions</th>
        </tr>
    </thead>
 </table>
```

##Tooltip

```html
<a href='url' tooltip='View' data-placement="left">View</a>
```
>http://getbootstrap.com/javascript/#tooltips