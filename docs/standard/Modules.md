#Modules

```javascript
app
 |-- Modules
      |-- PA // Application Code
           |-- Controllers // Controllers
           |-- Helpers // Fungsi tanpa class / namespace
           |-- Migrations // migration definition
           |-- Resources // Custom library
           |--  |-- views // view aplikasi
           |--  |-- assets // Resource asset aplikasi (Javascript, CSS, Images, Font, dll)
           |-- / routes.php / // Route aplikasi
           |-- / gulpfile.js / // custom gulp khusus aplikasi #optional
```
