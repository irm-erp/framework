#Comment

Beberapa programmer kadang merasa malas untuk menambahkan komentar pada program yang dibuatnya. Akan tetapi penggunaan komentar pada source code sangat dibutuhkan, apalagi pada source yang sangat panjang. 

Dalam menuliskan komentar, anda tidak perlu menambahkannya pada setiap line baris kode. Cukup pada bagian yang dirasa perlu diberi keterangan saja.

Contoh:

```php
$isZip = false;

// build path for requested files
$requestedFiles = $req->input("path", []);
$requestedFiles = array_filter(array_map(function($item) use (&$isZip) {
  if (!Storage::exists($item))
    return;

  $requestedFiles = Drive::path($item);

  // will create archive file if path is folder
  if (is_dir($requestedFiles))
    $isZip = true;

  return $requestedFiles;
}, is_array($requestedFiles) ? $requestedFiles : [$requestedFiles]));

$filesCount = count($requestedFiles);
$isZip = $isZip || $filesCount > 1;

// invalid download request
if ($filesCount == 0) 
  throw new ServiceException("Request failed.");

// return zipped file if multiple
if ($isZip) {
  $file = date("YmdHis-").preg_replace("/[\.\s]/", "", microtime()).rand().".zip";
  $filepath = Drive::path($file, "tmp");

  // create archive file
  Zippy::load()->create($filepath, $requestedFiles);

  // get file content and make download response
  $response = response(file_get_contents($filepath))
          ->header("Content-Length", filesize($filepath));

  // delete zip file
  Storage::disk("tmp")->delete($file);

  // return download response
  return $response
      ->header("Content-Description", "File Transfer")
      ->header("Content-Type", "application/octet-stream")
      ->header("Content-Transfer-Encoding", "binary")
      ->header("Expires", "0")
      ->header("Cache-Control", "must-revalidate")
      ->header("Content-Disposition", "attachment; filename=document-".date("Y-m-d").".zip");
}

// return single file to download
return response()->download(current($requestedFiles));
```

#Fungsi & Method
Selalu gunakan komentar pada setiap definisi fungsi. Jelaskan kegunaan fungsi tersebut, tipe data parameter beserta outputnya. 

Contoh:

```php
/**
 * Convert angka ke format currency
 * 
 * @param  integer  $value     
 * @param  integer  $decimals  jumlah angka belakang koma
 * @param  string   $currency  format currency
 * @return string
 */
function currency($value, $decimals=0, $currency="Rp") 
{
    return "$currency ".number_format($value, $decimals);
}
```