<?php 

namespace App\Models;

use DB;

class User extends Model
{
    protected $fillable = ["name", "email", "password", "tenant_code"];

    protected $hidden = ["password", "remember_token"];

    public function rules()
    {
        return [
            "name" => "required|max:255",
            "email" => "required|email|max:255",
        ];
    }

    public function registerRoles(array $roles) 
    {
        DB::transaction(function() use ($roles) {
            $data = [];
            $id = $this->getKey();

            foreach ($roles as $role_id) 
                $data[] = ["role_id" => $role_id, "user_id" => $id];

            DB::table("role_user")->where("user_id", $id)->delete();
            DB::table("role_user")->insert($data);
        });
    }
}