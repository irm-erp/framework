<?php

namespace App\Http\Middleware;

use Closure;

class RetrieveRouteParameter
{
    private $key = "param.router";

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $parameters = array_slice(func_get_args(), 2);
        $route_params = $request->route()->parameters;
        $data = [];

        foreach ($parameters as $param) {
            $data[$param] = @$route_params[$param];
            $request->route()->forgetParameter($param);
        }

        config([$this->key => $data]);

        return $next($request);
    }
}
