<?php

use Illuminate\Database\Seeder;
use Laraden\Seeds\UserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::transaction(function() {
	        $this->call(UserSeeder::class);
	    });
    }
}
